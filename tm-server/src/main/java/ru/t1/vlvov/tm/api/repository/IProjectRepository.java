package ru.t1.vlvov.tm.api.repository;

import ru.t1.vlvov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

}
