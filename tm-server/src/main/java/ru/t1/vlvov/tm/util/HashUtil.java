package ru.t1.vlvov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.component.ISaltProvider;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public interface HashUtil {

    @Nullable
    static String salt(@Nullable final String value, @Nullable final String secret, @Nullable final Integer iteration) {
        if (value == null || secret == null || iteration == null) return null;
        @NotNull String result = value;
        for (int i = 0; i < iteration; i++) {
            result = sha256(secret + result + secret);
        }
        return result;
    }

    @NotNull
    static String sha256(@NotNull final String base) {
        try {
            @NotNull final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(base.getBytes(StandardCharsets.UTF_8));
            @NotNull final StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                @NotNull final String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (@NotNull final Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Nullable
    static String salt(@Nullable final String value, @Nullable ISaltProvider saltProvider) {
        if (saltProvider == null) return null;
        @NotNull final String secret = saltProvider.getPasswordSecret();
        @NotNull final Integer iteration = saltProvider.getPasswordIteration();
        return salt(value, secret, iteration);
    }

}
