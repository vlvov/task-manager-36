package ru.t1.vlvov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.service.IAuthService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.api.service.ISessionService;
import ru.t1.vlvov.tm.api.service.IUserService;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.exception.entity.UserNotFoundException;
import ru.t1.vlvov.tm.exception.field.LoginEmptyException;
import ru.t1.vlvov.tm.exception.field.PasswordEmptyException;
import ru.t1.vlvov.tm.exception.user.AccessDeniedException;
import ru.t1.vlvov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.vlvov.tm.exception.user.PermissionException;
import ru.t1.vlvov.tm.model.Session;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.util.CryptUtil;
import ru.t1.vlvov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Date;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService,
            @NotNull final ISessionService sessionService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @Nullable
    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public String login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.isLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password, propertyService);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        return getToken(user);
    }

    @NotNull
    private String getToken(@NotNull final User user) {
        return getToken(createSession(user));
    }

    @NotNull
    private Session createSession(@NotNull final User user) {
        @NotNull Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        sessionService.add(session);
        return session;
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final Session session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    @SneakyThrows
    public Session validateToken(@Nullable final String token) {
        if (token == null || token.equals("")) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(json, Session.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        if (!sessionService.existsById(session.getId())) throw new AccessDeniedException();
        return session;
    }

    @Override
    @SneakyThrows
    public void logout(@Nullable final String token) {
        if (token == null || token.equals("")) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(json, Session.class);
        sessionService.remove(session);
    }

}
