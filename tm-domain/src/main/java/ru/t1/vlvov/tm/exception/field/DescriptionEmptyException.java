package ru.t1.vlvov.tm.exception.field;

public final class DescriptionEmptyException extends AbstractFieldNotFoundException {

    public DescriptionEmptyException() {
        super("Error! Description is empty...");
    }

}
